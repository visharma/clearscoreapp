//
//  ClearScoreAppTests.swift
//  ClearScoreAppTests
//
//  Created by Vishal Sharma on 11/19/22.
//

import XCTest
import Combine
@testable import ClearScoreApp

class MockCreditService : ClearCreditServiceProtocol {
    var fetchCreditResult: AnyPublisher<ClearCreditModel, Error>!
    
    func getCreditData() -> AnyPublisher<ClearCreditModel, Error> {
       return fetchCreditResult
    }
}

final class ClearScoreTests: XCTestCase {
    private var viewModel: ClearCreditViewModel!
    private var mockService: MockCreditService!
    private var cancellables: Set<AnyCancellable> = []

    override func setUpWithError() throws {
        try super.setUpWithError()
        mockService = MockCreditService()
        viewModel = ClearCreditViewModel(service: mockService)
    }

    override func tearDownWithError() throws {
        mockService = nil
        viewModel = nil
        try super.tearDownWithError()
    }

    func testModelParser() {
        guard let url = Bundle.main.url(forResource: "MockData", withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
            return XCTAssertNil("Unable to fetch data")
        }
        
        let creditInfo = try! JSONDecoder().decode(ClearCreditModel.self, from: data)
        XCTAssertNotNil(creditInfo)
        XCTAssertEqual(creditInfo.creditReportInfo.score, 514)
    }
    
    func testFetchCreditDetails() {
        let reportInfo = ReportInfo(score: 700, percentageCreditUsed: 10, equifaxScoreBandDescription: "Excellent", equifaxScoreBand: 5)
        let creditModel = ClearCreditModel(creditReportInfo: reportInfo)
        let expectation = XCTestExpectation(description: "Credit details are fetched")

        viewModel.$creditScore.dropFirst().sink { state in
            XCTAssertEqual(state, 700)
            expectation.fulfill()
        }
        .store(in: &cancellables)
        
        viewModel.$percentageCreditUsed.dropFirst().sink { state in
            XCTAssertEqual(state, 10)
            expectation.fulfill()
        }
        .store(in: &cancellables)

        mockService.fetchCreditResult = Result.success(creditModel).publisher.eraseToAnyPublisher()
        viewModel.fetchCreditDetails()
   
        wait(for: [expectation], timeout: 1)
    }
    
    func testEmptyCreditDetails() {
        let reportInfo = ReportInfo(score: 0, percentageCreditUsed: 0, equifaxScoreBandDescription: "", equifaxScoreBand: 0)
        let creditModel = ClearCreditModel(creditReportInfo: reportInfo)
        let expectation = XCTestExpectation(description: "Credit details set to empty")

        viewModel.$creditScore.dropFirst().sink { state in
            XCTAssertEqual(state, 0)
            expectation.fulfill()
        }
        .store(in: &cancellables)

        mockService.fetchCreditResult = Result.success(creditModel).publisher.eraseToAnyPublisher()
        viewModel.fetchCreditDetails()
   
        wait(for: [expectation], timeout: 1)
    }
}
