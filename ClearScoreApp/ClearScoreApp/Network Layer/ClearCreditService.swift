//
//  ClearCreditService.swift
//  ClearScoreApp
//
//  Created by Vishal Sharma on 11/19/22.
//

import Foundation
import Combine

protocol ClearCreditServiceProtocol {
    func getCreditData() -> AnyPublisher<ClearCreditModel, Error>
}

class ClearCreditService: ClearCreditServiceProtocol {
    
    func getCreditData() -> AnyPublisher<ClearCreditModel, Error> {
        let request = URL(
            string: "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod/mockcredit/values"
        )
        
        return ClearApiManager.shared.callAPI(request!)
            .map(\.value)
            .eraseToAnyPublisher()
    }
}
