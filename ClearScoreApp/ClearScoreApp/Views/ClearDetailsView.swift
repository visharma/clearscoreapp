//
//  ClearDetailsView.swift
//  ClearScoreApp
//
//  Created by Vishal Sharma on 11/19/22.
//

import SwiftUI

struct ListRows: View {
    let title: LocalizedStringKey
    let type: String

    var body: some View {
        HStack {
            Text(title)
                .font(.headline)
            Spacer()
            Text(type)
                .font(.headline)
                .foregroundColor(.secondary)
        }
    }
}

struct ClearDetailsView: View {
    @ObservedObject var creditViewModel: ClearCreditViewModel
    
    var body: some View {
        NavigationView {
            List {
                ListRows(
                    title: LocalizedStringKey("ScoreText"),
                    type: "\(creditViewModel.creditScore)"
                )
                ListRows(
                    title: LocalizedStringKey("PercentageUsedText"),
                    type:"\(creditViewModel.percentageCreditUsed)"
                )
                ListRows(
                    title: LocalizedStringKey("EquifaxScoreText"),
                    type: "\(creditViewModel.equifaxScoreBand)"
                )
                ListRows(
                    title: LocalizedStringKey("BandDescription"),
                    type: "\(creditViewModel.bandDescription)"
                )
            }
            .listStyle(.grouped)
        }
        .toolbar {
            ToolbarItem(placement: .principal) {
                Text(LocalizedStringKey("DetailsTitle"))
                    .foregroundColor(.white)
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .toolbarBackground(.black, for: .navigationBar)
        .toolbarBackground(.visible, for: .navigationBar)
    }
}

struct ClearDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        ClearDetailsView(creditViewModel: ClearCreditViewModel())
    }
}
