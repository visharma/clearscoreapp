//
//  ContentView.swift
//  ClearScoreApp
//
//  Created by Vishal Sharma on 11/19/22.
//

import SwiftUI

struct ClearDashboardView: View {
    @EnvironmentObject var router: Router<Path>
    @ObservedObject var creditViewModel: ClearCreditViewModel
    
    var body: some View {
        NavigationView {
            ZStack {
                Circle()
                    .stroke(Color.black.opacity(0.3), style: StrokeStyle(lineWidth: 5))
                
                Circle()
                    .trim(from: 0, to: self.creditViewModel.creditScale)
                    .stroke(Color.green, style: StrokeStyle(lineWidth: 10))
                    .rotationEffect(Angle(degrees: -90))
                    .animation(
                        .linear(duration: 1),
                        value: self.creditViewModel.creditScale
                    )
                VStack {
                    Text(LocalizedStringKey("HeaderText"))
                    Text("\(self.creditViewModel.creditScore)")
                        .font(.largeTitle)
                        .foregroundColor(.green)
                    Text(LocalizedStringKey("SubtitleText"))
                }
            }
            .onTapGesture {
                self.router.push(.details)
            }
            .padding(70)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text(LocalizedStringKey("DashboardTitle"))
                        .foregroundColor(.white)
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbarBackground(.black, for: .navigationBar)
            .toolbarBackground(.visible, for: .navigationBar)
            .onAppear {
                self.creditViewModel.fetchCreditDetails()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ClearDashboardView(creditViewModel: ClearCreditViewModel())
    }
}
