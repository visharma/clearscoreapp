//
//  ClearRoutingView.swift
//  ClearScoreApp
//
//  Created by Vishal Sharma on 11/19/22.
//

import SwiftUI

enum Path {
    case dashboard
    case details
}

final class Router<T: Hashable>: ObservableObject {
    @Published var paths: [T] = []
    func push(_ path: T) {
        paths.append(path)
    }
}

struct ClearRoutingView: View {
    @ObservedObject var router = Router<Path>()
    @StateObject var creditViewModel = ClearCreditViewModel()
    
    var body: some View {
        NavigationStack(path: $router.paths) {
            ClearDashboardView(creditViewModel: creditViewModel)
                .navigationDestination(for: Path.self) { path in
                    switch path {
                    case .dashboard:
                        ClearDashboardView(creditViewModel: creditViewModel)
                    case .details:
                        ClearDetailsView(creditViewModel: creditViewModel)
                    }
                }
        }
        .environmentObject(router)
    }
}

struct ClearRoutingView_Previews: PreviewProvider {
    static var previews: some View {
        ClearRoutingView()
    }
}
