//
//  ClearCreditViewModel.swift
//  ClearScoreApp
//
//  Created by Vishal Sharma on 11/19/22.
//

import Foundation
import Combine

class ClearCreditViewModel: ObservableObject {
    
    //Output
    @Published var creditScore = 0
    @Published var creditScale = 0.0
    @Published var percentageCreditUsed = 0
    @Published var bandDescription = ""
    @Published var equifaxScoreBand = 0
    
    private var disposables = Set<AnyCancellable>()
    private let creditService: ClearCreditServiceProtocol
    
    init(service: ClearCreditServiceProtocol = ClearCreditService()) {
        self.creditService = service
    }
    
    func fetchCreditDetails() {
        let _ = self.creditService.getCreditData()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (_) in
                            
            }) { [weak self] (creditInfo) in
                guard let `self` = self else {
                    return
                }
                self.creditScore = creditInfo.creditReportInfo.score
                self.creditScale = Double(creditInfo.creditReportInfo.score) / Double(700)
                self.percentageCreditUsed = creditInfo.creditReportInfo.percentageCreditUsed
                self.bandDescription = creditInfo.creditReportInfo.equifaxScoreBandDescription
                self.equifaxScoreBand = creditInfo.creditReportInfo.equifaxScoreBand
            }
            .store(in: &disposables)
    }
}

