//
//  ClearCreditModel.swift
//  ClearScoreApp
//
//  Created by Vishal Sharma on 11/19/22.
//

import Foundation

struct ClearCreditModel: Codable {
    let creditReportInfo: ReportInfo
}

struct ReportInfo: Codable {
    let score: Int
    let percentageCreditUsed: Int
    let equifaxScoreBandDescription: String
    let equifaxScoreBand: Int
}
