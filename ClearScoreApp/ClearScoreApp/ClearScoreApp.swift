//
//  ClearScoreAppApp.swift
//  ClearScoreApp
//
//  Created by Vishal Sharma on 11/19/22.
//

import SwiftUI

@main
struct ClearScoreApp: App {
    var body: some Scene {
        WindowGroup {
            ClearRoutingView()
        }
    }
}
