# ClearScoreApp

## Code Design 
The code is designed using SwiftUI, Combine with MVVM design pattern. Navigation stack is implemented with Router approach. A generic Network layer is added to handle Api calls. Service call is implemented using protocol based approach and dependency injection with VM layer to support mocking and testable code. 

## Given more time
I'd have added error handling mechanism and further scale up unit test cases. 
